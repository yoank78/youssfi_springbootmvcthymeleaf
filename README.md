# youssfi_springBootMvcThymeleaf

## Projet pour dérouler le lab : Spring Boot Spring MVC Thymeleaf Spring Data Intellij
- Part1 : https://www.youtube.com/watch?v=Zaf3XIP2wss
- Part2 : https://www.youtube.com/watch?v=eoBE745lDE0
- Part3 : https://www.youtube.com/watch?v=Ejdeqs4rWek
- Part4 : https://www.youtube.com/watch?v=Zrq1_-EX0XM

## Resources
https://spring.io/guides/topicals/spring-security-architecture
https://howtodoinjava.com/spring-security/inmemory-jdbc-userdetails-service/

Autres:
https://ippon26.rssing.com/chan-40092195/all_p24.html

## Démarrer l'application depuis un Terminal
cd patients-mvc
./mvnw spring-boot:run

## Parcours des vidéos
### Part1
- 00:00:35 : création du projet Spring Boot -> (v2.7)
Java 11 au lieu de Java 8 dans la vidéo
Dépendances :
- Spring Data JPA
- H2 Database
- Lombok
- Spring Web
- Thymeleaf

- 00:05:30 : faut-il ajouter l'annotation NoArgsConstructor pour notre entité JPA ? (une entité JPA exige un constructeur sans paramètre qui est public)
- 00:06:50 : création interface PatientRepository
- 00:07:20 : création CommandLineRunner pour tester l'application (ajout de patient en bdd + recherche findAll)
- 00:09:45 : configuration database dans le application.properties
- 00:11:15 : accès console h2 (ajout de la propriété spring.h2.console.settings.web-allow-others=true pour pouvoir y accèder depuis une machine distante)
- 00:11:45 : création Controller pour avoir un accès web

- 00:22:25 : intégration de bootstrap

- 00:30:00 : modification bdd H2 -> Mysql
Changement de l'image de base Gitpod utilisée pour les dev pour avoir une bdd MySQL disponible dans le workspace
Voir : https://www.gitpod.io/blog/gitpodify/#mysql
Modificataions dans : .gitpod.yml et .gitpod.dockerfile


### Part2
- 00:04:50 : ajout dépendance Thymeleaf (pom.xml) pour créer des templates de page
- 00:15:00 : création template et utilisation dans la page patients.html
- 00:27:30 : création nouvelle page pour le formulaire de création d'un nouveau patient
- 00:45:15 : gestion de la validation des données (ajout dépendance -> spring-boot-starter-validation)

### Part3 : ajout couche de sécurité pour accèder à l'application (Spring Security)
- 00:02:35 : ajout dépendance Spring Security (pom.xml)
- 00:08:30 : création classe de configuration SecurityConfig (class annotée @Configuration démarrées en 1ere par Spring)
    à noter que la classe utilisée est deprecated, voir : https://spring.io/blog/2022/02/21/spring-security-without-the-websecurityconfigureradapter
- 00:20:00 : gestion des utilisateurs InMemory
- 00:25:00 : Thymeleaf avec Spring Security, ajout dépendance : 
/!\ Voir le PB du user connecté qui correspond au dernier user InMemory et pas celui connecté /!\
Le PB venait de la configuration de Spring Security, il ne fallait pas mettre httpBasic()
https://stackoverflow.com/questions/5023951/spring-security-unable-to-logout

- 00:43:00 : ajout controleur pour gérer les exceptions pour les accès interdits http 403
- 01:00:30 : refactor des URL pour une gestion simplifiée de la gestion d'accès définie dans la config de Sécurité
- 01:07:50 : Gestion de l'authentification via JDBC (2ème stratégie d'authentification)
- 01:20:00 : @todo: reprendre la configuration de la sécurité pour se connecter à la BDD !!!

Article sur le sujet : https://www.baeldung.com/spring-security-jdbc-authentication
Queslques commandes pour utiliser MySQL
https://dev.mysql.com/doc/refman/8.0/en/database-use.html
mysql> SHOW DATABASES;
mysql> USE PATIENTS_DB;
mysql> SHOW TABLES;
mysql> CREATE TABLE pet (name VARCHAR(20), owner VARCHAR(20), species VARCHAR(20), sex CHAR(1), birth DATE, death DATE);

Création de 3 tables
users
- username VARCHAR(15) PK
- password VARCHAR(255)
- active TINYINT
mysql> CREATE TABLE users (username VARCHAR(15) PRIMARY KEY, password VARCHAR(255), active TINYINT);

roles
- role VARCHAR(15) PK
mysql> CREATE TABLE roles (role VARCHAR(15) PRIMARY KEY);

users_roles
- username VARCHAR(15) PK
- role VARCHAR(15) PK
mysql> CREATE TABLE users_roles (username VARCHAR(15), role VARCHAR(15), PRIMARY KEY(username, role));


INSERT INTO users VALUES('user1', '$2a$10$W4gWdTdWoTWp3pj2h8Oh0e.tz1bpVqROEYzGx2MJLZF4354yxuunO', 1);
INSERT INTO users VALUES('user2', '$2a$10$W4gWdTdWoTWp3pj2h8Oh0e.tz1bpVqROEYzGx2MJLZF4354yxuunO', 1);
INSERT INTO users VALUES('admin', '$2a$10$W4gWdTdWoTWp3pj2h8Oh0e.tz1bpVqROEYzGx2MJLZF4354yxuunO', 1);

INSERT INTO roles VALUES('USER');
INSERT INTO roles VALUES('ADMIN');

INSERT INTO users_roles VALUES('user1', 'USER');
INSERT INTO users_roles VALUES('user2', 'USER');
INSERT INTO users_roles VALUES('admin', 'USER');
INSERT INTO users_roles VALUES('admin', 'ADMIN');


## Part4 : 3ème stratégie d'authentification basée sur UserDetailService au lieu de Jdbc (https://www.youtube.com/watch?v=Zrq1_-EX0XM)
https://www.baeldung.com/spring-security-authentication-provider
https://www.javadevjournal.com/spring-security/spring-security-custom-authentication-provider/

- 00:03:10 : accès à l'application sans être authentifié => ajout permitAll() sur les resources statiques
- 00:07:50 : passage de jdbcAuthentication à 
- 00:10:00 : création des entités pour stocker les données utilisateurs
- 00:52:55 : retour sur la config Sécurité
- 00:52:55 : 

## original template

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/yoank78/youssfi_springbootmvcthymeleaf.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/yoank78/youssfi_springbootmvcthymeleaf/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
