package bzh.learning.youssfi.patientsmvc.patientsmvc.security.service;

import java.util.UUID;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import bzh.learning.youssfi.patientsmvc.patientsmvc.security.entities.AppRole;
import bzh.learning.youssfi.patientsmvc.patientsmvc.security.entities.AppUser;
import bzh.learning.youssfi.patientsmvc.patientsmvc.security.repositories.AppRoleRepository;
import bzh.learning.youssfi.patientsmvc.patientsmvc.security.repositories.AppUserRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Transactional
public class SecurityServiceImpl implements SecurityService {

    private AppUserRepository appUserRepository;
    private AppRoleRepository appRoleRepository;
    private PasswordEncoder passwordEncoder;

    public SecurityServiceImpl(AppUserRepository appUserRepository, AppRoleRepository appRoleRepository, PasswordEncoder passwordEncoder) {
        this.appRoleRepository = appRoleRepository;
        this.appUserRepository = appUserRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public AppUser saveNewUser(String username, String password1, String password2) {
        if (!password1.equals(password2)) {
            throw new RuntimeException("Password not match");
        }
        String hashedPwd = passwordEncoder.encode(password1);

        AppUser appUser = new AppUser();
        appUser.setUserId(UUID.randomUUID().toString());
        appUser.setUsername(username);
        appUser.setPassword(hashedPwd);
        appUser.setActive(true);
        
        AppUser savedAppUser = appUserRepository.save(appUser);
        return savedAppUser;
    }

    @Override
    public AppRole saveNewRole(String roleName, String description) {
        // Check if appRole already exists in database
        AppRole appRole = appRoleRepository.findByRoleName(roleName);
        if (appRole != null) {
            throw new RuntimeException("Role [" + roleName + "] already exists in database");
        }

        appRole = new AppRole();
        appRole.setRoleName(roleName);
        appRole.setDescription(description);

        AppRole savedAppRole = appRoleRepository.save(appRole);
        return savedAppRole;
    }

    @Override
    public void addRoleToUser(String username, String roleName) {
        // Get appUser from database
        AppUser appUser = appUserRepository.findByUsername(username);
        if (appUser == null) {
            throw new RuntimeException("User [" + username + "] not found");
        }

        // Get appRole from database
        AppRole appRole = appRoleRepository.findByRoleName(roleName);
        if (appRole == null) {
            throw new RuntimeException("Role [" + roleName + "] not found");
        }

        appUser.getAppRoles().add(appRole);
    }

    @Override
    public void removeRoleFromUser(String username, String roleName) {
        // Get appUser from database
        AppUser appUser = appUserRepository.findByUsername(username);
        if (appUser == null) {
            throw new RuntimeException("User [" + username + "] not found");
        }

        // Get appRole from database
        AppRole appRole = appRoleRepository.findByRoleName(roleName);
        if (appRole != null) {
            throw new RuntimeException("Role [" + roleName + "] not found");
        }

        appUser.getAppRoles().remove(appRole);
    }

    @Override
    public AppUser loadUserByUserName(String username) {
        return appUserRepository.findByUsername(username);
    }
}
