package bzh.learning.youssfi.patientsmvc.patientsmvc.security;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

import bzh.learning.youssfi.patientsmvc.patientsmvc.security.service.CustomAuthenticationProvider;
import bzh.learning.youssfi.patientsmvc.patientsmvc.security.service.UserDetailsServiceImpl;

@Configuration
@EnableWebSecurity
public class SecurityConfig {

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        System.out.println("DEBUG - filterChain()");
        http.formLogin()
        .and()
/*         .authorizeHttpRequests((authz) -> authz
                .anyRequest().authenticated()
            ) */
            .authorizeHttpRequests().antMatchers("/").permitAll()
        .and()
            .authorizeHttpRequests().antMatchers("/webjars/**").permitAll()
        .and()
            .authorizeHttpRequests().antMatchers("/admin/**").hasAuthority("ADMIN")
        .and()
            .authorizeHttpRequests().antMatchers("/user/**").hasAuthority("USER")
        .and()
            .authorizeHttpRequests((authz) -> authz
                .anyRequest().authenticated()
            )
        .exceptionHandling().accessDeniedPage("/403")
/*             .httpBasic(withDefaults()); */
            ;

        return http.build();
    }

    // @Bean
    // public UserDetailsService userDetailsService() {
    //     UserDetails user1 = User.withUsername("user1")
    //         .password(passwordEncoder().encode("1234"))
    //         .roles("USER")
    //         .build();
    //     System.out.println("user1.passwword="+user1.getPassword());

    //     UserDetails user2 = User.withUsername("admin")
    //         .password(passwordEncoder().encode("1234"))
    //         .roles("USER", "ADMIN")
    //         .build();
    //     System.out.println("user2.passwword="+user2.getPassword());

    //     UserDetails user3 = User.withUsername("user3")
    //         .password(passwordEncoder().encode("1234"))
    //         .roles("USER")
    //         .build();
    //     System.out.println("user3.passwword="+user3.getPassword());

    //     UserDetails user4 = User.withUsername("user4")
    //         .password(passwordEncoder().encode("1234"))
    //         .roles("USER", "ADMIN")
    //         .build();
    //     System.out.println("user4.passwword="+user4.getPassword());

    //     auth.jdb

    //     return new InMemoryUserDetailsManager(user1, user2, user3, user4);
    // }


    // @Bean
	// public UserDetailsManager userDetails(DataSource dataSource) {
    
    //     JdbcUserDetailsManager users = new JdbcUserDetailsManager(dataSource);
    //     users.setUsersByUsernameQuery("SELECT username AS principal, password AS credentials, active FROM users WHERE username=?");
    //     users.setAuthoritiesByUsernameQuery("SELECT username AS principal, role AS role FROM users_roles WHERE username=?");
    //     users.setRolePrefix("ROLE_");
    
    //     UserDetails user = users.loadUserByUsername("user1");
    //     System.out.println("DEBUG - recup user1 : " + user.getUsername() + ", enabled : " + user.isEnabled());
    //     user.getAuthorities().forEach(auth->System.out.println("Authority : " + auth.getAuthority()));

    //     user = users.loadUserByUsername("user2");
    //     System.out.println("DEBUG - recup user2 : " + user.getUsername() + ", enabled : " + user.isEnabled());
    //     user.getAuthorities().forEach(auth->System.out.println("Authority : " + auth.getAuthority()));

    //     user = users.loadUserByUsername("admin");
    //     System.out.println("DEBUG - recup admin : " + user.getUsername() + ", enabled : " + user.isEnabled());
    //     user.getAuthorities().forEach(auth->System.out.println("Authority : " + auth.getAuthority()));
    
    //     return users;
    // }
    
    // @Bean
    // AuthenticationManager customAuthenticationManager(DataSource dataSource) {
    //     LdapBindAuthenticationManagerFactory factory = 
    //         new LdapBindAuthenticationManagerFactory(contextSource);
    //     factory.setUserDnPatterns("uid={0},ou=people");
    //     factory.setUserDetailsContextMapper(new PersonContextMapper());
    //     return factory.createAuthenticationManager();
    // }

    @Bean
    public CustomAuthenticationProvider authProvider(UserDetailsService userDetailsService) {
        CustomAuthenticationProvider authenticationProvider = new CustomAuthenticationProvider(userDetailsService);
        return authenticationProvider;
    }
    
    // @Bean
	// public UserDetailsService userDetails(UserDetailsServiceImpl userDetailsServiceImpl) {
    //     return userDetailsServiceImpl;
    // }

    @Bean
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
