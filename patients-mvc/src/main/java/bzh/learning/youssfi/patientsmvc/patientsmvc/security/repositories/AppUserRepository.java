package bzh.learning.youssfi.patientsmvc.patientsmvc.security.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import bzh.learning.youssfi.patientsmvc.patientsmvc.security.entities.AppUser;

public interface AppUserRepository extends JpaRepository<AppUser, String> {
    AppUser findByUsername(String username);
}
