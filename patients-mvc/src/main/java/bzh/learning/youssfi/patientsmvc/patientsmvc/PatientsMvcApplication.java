package bzh.learning.youssfi.patientsmvc.patientsmvc;

import java.time.LocalDate;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import bzh.learning.youssfi.patientsmvc.patientsmvc.entities.Patient;
import bzh.learning.youssfi.patientsmvc.patientsmvc.repositories.PatientRepository;
import bzh.learning.youssfi.patientsmvc.patientsmvc.security.entities.AppRole;
import bzh.learning.youssfi.patientsmvc.patientsmvc.security.entities.AppUser;
import bzh.learning.youssfi.patientsmvc.patientsmvc.security.service.SecurityService;

@SpringBootApplication
public class PatientsMvcApplication {

	public static void main(String[] args) {
		SpringApplication.run(PatientsMvcApplication.class, args);
	}

	//@Bean
	CommandLineRunner commandLineRunner(PatientRepository patientRepository) {
		return args -> {
			patientRepository.save(new Patient(null, "John", LocalDate.now(), false, 12));
			patientRepository.save(new Patient(null, "David", LocalDate.now(), false, 7));
			patientRepository.save(new Patient(null, "Corentin", LocalDate.now(), true, 1));
			patientRepository.save(new Patient(null, "Magalie", LocalDate.now(), false, 88));

			patientRepository
				.findAll()
				.forEach(p -> {System.out.println(p.getNom());});
		};
	}

	// @Bean
	CommandLineRunner commandLineRunner(SecurityService securityService) {
		return args -> {
			AppUser appUserBob = securityService.saveNewUser("bob", "1234", "1234");
			AppUser appUserDidier = securityService.saveNewUser("didier", "1234", "1234");
			AppUser appUserDede = securityService.saveNewUser("dede", "1234", "1234");

			AppRole appRoleUser = securityService.saveNewRole("USER", "Basic user");
			AppRole appRoleAdmin = securityService.saveNewRole("ADMIN", "Admin user");

			securityService.addRoleToUser(appUserBob.getUsername(), appRoleUser.getRoleName());
			securityService.addRoleToUser(appUserBob.getUsername(), appRoleAdmin.getRoleName());
			securityService.addRoleToUser(appUserDidier.getUsername(), appRoleUser.getRoleName());
			securityService.addRoleToUser(appUserDede.getUsername(), appRoleUser.getRoleName());

		};
	}
}
