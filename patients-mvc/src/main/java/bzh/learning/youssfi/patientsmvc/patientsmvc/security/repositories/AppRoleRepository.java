package bzh.learning.youssfi.patientsmvc.patientsmvc.security.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import bzh.learning.youssfi.patientsmvc.patientsmvc.security.entities.AppRole;

public interface AppRoleRepository extends JpaRepository<AppRole, Long> {
    AppRole findByRoleName(String roleName);
}
