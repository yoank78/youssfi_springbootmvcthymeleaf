package bzh.learning.youssfi.patientsmvc.patientsmvc.security.service;

import bzh.learning.youssfi.patientsmvc.patientsmvc.security.entities.AppRole;
import bzh.learning.youssfi.patientsmvc.patientsmvc.security.entities.AppUser;

public interface SecurityService {
    AppUser saveNewUser(String username, String password1, String password2);
    AppRole saveNewRole(String roleName, String description);
    void addRoleToUser(String username, String roleName);
    void removeRoleFromUser(String username, String roleName);
    AppUser loadUserByUserName(String username);
}
